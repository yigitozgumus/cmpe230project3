/********************************************************************************
** Form generated from reading UI file 'addfolderdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDFOLDERDIALOG_H
#define UI_ADDFOLDERDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AddFolderDialog
{
public:
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *lineEdit;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *AddFolderDialog)
    {
        if (AddFolderDialog->objectName().isEmpty())
            AddFolderDialog->setObjectName(QStringLiteral("AddFolderDialog"));
        AddFolderDialog->resize(400, 100);
        widget = new QWidget(AddFolderDialog);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(20, 10, 351, 71));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(widget);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        lineEdit = new QLineEdit(widget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        horizontalLayout->addWidget(lineEdit);


        verticalLayout->addLayout(horizontalLayout);

        buttonBox = new QDialogButtonBox(widget);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(AddFolderDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), AddFolderDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), AddFolderDialog, SLOT(close()));

        QMetaObject::connectSlotsByName(AddFolderDialog);
    } // setupUi

    void retranslateUi(QDialog *AddFolderDialog)
    {
        AddFolderDialog->setWindowTitle(QApplication::translate("AddFolderDialog", "Add Folder", 0));
        label->setText(QApplication::translate("AddFolderDialog", "Full Path of the Folder", 0));
    } // retranslateUi

};

namespace Ui {
    class AddFolderDialog: public Ui_AddFolderDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDFOLDERDIALOG_H
