#!/usr/bin/perl
use Imagga ;
#file name
$keyFile = "./KeysAndSettings.txt";
open(TF,'<',$keyFile);
my $apiKey ;
my $apiSecret ;
my $filePath = $ARGV[1];
#take the access and the secret key
while(<TF>){
	if($. == 3){
	my @arrOp = split( /"/, $_) ;
	$apiKey = $arrOp[1];
}	elsif($. == 4){
	my @arrSec = split (/"/, $_) ;
	$apiSecret = $arrSec[1];
}
}
#create imagga object
$imagga = new imagga($apiKey, $apiSecret);

my( $err, $hash ) = $imagga->tag_image($filePath);

print "Error: ".$err."\n" if($err != 0);
#whole label array
my @arryLabels;
for (sort {$hash->{$b} <=> $hash->{$a}} keys %$hash){
	push(@arryLabels, $_ );
}
#take with the the greatest confidence values in the number of argument 
for(my $ind =0; $ind < $ARGV[0];$ind++){
	print "$arryLabels[$ind]\n";	
}

