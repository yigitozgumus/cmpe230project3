#!/usr/bin/perl
#folders and variables
$galleryPath = "./Gallery.txt";
$downloadPath = "./DownloadedSamples.txt";
$labelPath = "./LabelsOnly.txt";
$settingsFile = "./KeysAndSettings.txt";
$labelQuantity;
$sampleQuantity;
open($settings,'<',$settingsFile);
#take the sample and label numbers
while(<$settings>){
	if($. == 5){
	my @arrL = split( /"/, $_) ;
	$labelQuantity = $arrL[1];	
}	elsif($. == 6){
	my @arrS = split (/"/, $_) ;
	$sampleQuantity = $arrS[1];	
}
}
close $settings;
if($ARGV[0] eq "--listlabels"){
	#Using hash structure keep the label file unique
	open(my $uniqs,'<',$labelPath);
	my @labelUnique = <$uniqs>; close $uniqs ;
	@labelUnique = do { my %seen; grep { !$seen{$_}++ } @labelUnique };
	open(my $uniqs,'>',$labelPath);
	print $uniqs @labelUnique ; close $uniqs;
	print @labelUnique;
	#list all the images that the Gallery.txt file has
	}elsif($ARGV[0] eq "--listimages"){
		my $size = @ARGV;
		open($get,'<',$galleryPath);
		my @allPics = <$get>; close $get;
		if($size eq 1){
		#usage 2
			my @Images;
			for (@allPics){ push (@Images,$_ =~m/\[(.*?)\</);}
			@Images = do { my %seen; grep { !$seen{$_}++ } @Images };
			print join("\n",@Images)."\n" ;
		#usage 3 
		}elsif($size eq 2){
			open (my $imL,'<', $galleryPath);
			my @labelImages = <$imL>;
			my @Images ; 
			my $label = $ARGV[1];
			for (@labelImages){
			#if contains the label push to the array
			if($_ =~ m/<.*$label.*>/) {push (@Images, $_=~m/\[(.*?)\</);}
			}
			@Images = do { my %seen; grep { !$seen{$_}++ } @Images };
			print join("\n",@Images)."\n"; 
		#usage 4
		}elsif($size eq 3){
			open (my $imL,'<', $galleryPath);
			my @allImages = <$imL>;
			my $thePicture = $ARGV[2];
			my @labelWith;
			for(@allImages){
			#find the label(s) of the picture
			if($_ =~ m/\[$thePicture</){push (@labelWith, $_ =~m/<(.*?)>/);}
			}
			#print "@labelWith\n";
			my @labels = split(" ",@labelWith[0]);
	#print "@labels\n";
	my $sze = @labels;
	my $filter;
	if($sze >1){
		$filter = join("|",@labels);
	#print "$filter\n";
	}else{
		$filter = $labels[0];
	}
	#using regex find the images that have same label(s) just like in usage 3
	my @relevantImages ;
	for (@allImages){
		if($_ =~ m/<.*($filter).*>/) {push (@relevantImages, $_=~m/\[(.*?)\</);}
	}	
	@relevantImages = grep {!/$thePicture/}@relevantImages;
	@relevantImages = do { my %seen; grep { !$seen{$_}++ } @relevantImages };
	print join("\n",@relevantImages)."\n";
}
#usage 5
}elsif($ARGV[0] eq "--listsamples"){	
	my @Images ; 
	my $label = $ARGV[1];
	# run searchlabels.pl script
	@Images = qx{perl searchlabels.pl $sampleQuantity $label};
	print @Images;
	my $imgS = @Images;
	open($toSample,'>>',$downloadPath);
	for(my $ind=0;$ind<$imgS;$ind++){
	print $toSample "[".$label."<".$Images[$ind];
}
close $toSample;
	#just in case uniquness check
	open(my $uniqs,'<',$downloadPath);
	my @fileUnique = <$uniqs>; close $uniqs ;
	@fileUnique = do { my %seen; grep { !$seen{$_}++ } @fileUnique };
	open(my $uniqs,'>',$downloadPath);
	print $uniqs @fileUnique ; close $uniqs;
#usage 6
}elsif($ARGV[0] eq "--add"){
	my $dir = $ARGV[1];
	#find picture locations
	my @newPics = `cd $dir && find \`pwd\` -type f `;
	my @labelResults;
	for(@newPics){
		#create labels for the pictures
		push(@labelResults, $test= qx{perl labelimage.pl $labelQuantity $_ } ); 
		$_ =~ s/\n//;
	}
	open(my $labels,'>>',$labelPath);
	print $labels @labelResults ; close $labels;
	for(@labelResults){ $_ =~ s/\n/ /g ;}
	open(my $toGallery,'>>',$galleryPath);
	$galSize = @newPics ;
	for(my $index = 0;$index < $galSize;$index++){
	#add the pictures and the labels as [path<label(s)>] lines
	print $toGallery "[".$newPics[$index]."<".$labelResults[$index].">]\n";
} close $toGallery;
		#check uniqueness
		open(my $uniqs,'<',$galleryPath);
		my @fileUnique = <$uniqs>; close $uniqs ;
		@fileUnique = do { my %seen; grep { !$seen{$_}++ } @fileUnique };
		open(my $uniqs,'>',$galleryPath);
		print $uniqs @fileUnique ; close $uniqs;
	print join("\n",@newPics)."\n are added to the gallery.\n";
}

