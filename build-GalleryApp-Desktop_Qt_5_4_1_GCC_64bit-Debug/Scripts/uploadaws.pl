#! /usr/bin/perl
#variables and file names
my $dns;
my $key;
my $dnsFile = "<./awsPublicDns.txt";
my $keyFile = "<./pemName.txt";

open(my $getDns,$dnsFile); chomp($dns=<$getDns>); close $getDns;
open(my $getKey,$keyFile); chomp($key=<$getKey>); close $getKey;

#sftp command connection
$startftp = "| sftp -oIdentityFile=$key ec2-user\@$dns";
open(FT, $startftp);
#minimum argument case
if(@ARGV == 3){
	my $pcDirName = $ARGV[0];
	my $awsDirName = $ARGV[2];
	#if it is a folder
	if(-e @ARGV[0] && ( -d @ARGV[0])){
	print FT "mkdir $awsDirName\n";
	print FT "mkdir $awsDirName/$pcDirName\n";
	print FT "put -r $pcDirName $awsDirName\n";
}else {
	#if it is a file
	print FT "mkdir $awsDirName\n";
	print FT "put $ARGV[0] $awsDirName\n";
}
# n number of file upload
} elsif(@ARGV > 3){
	$size = @ARGV ;
	print $size ;
	my $awsDirName = $ARGV[$size-1];
	for(my $index=0;$index < $size-2; $index++){
	print FT "put  $ARGV[$index] $awsDirName\n";
}
}
close FT;
