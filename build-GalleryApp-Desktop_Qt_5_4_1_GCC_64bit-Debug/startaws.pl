#! /usr/bin/perl
#necessary variables and files
my $isworking ;
my $id;
my $dns;
my $dnsFile = "./awsPublicDns.txt";
my $runningFile = "./runningInstances.txt";
my $awsAccess ;
my $awsSecret;
$settingsFile = "./KeysAndSettings.txt";
open($settings,'<',$settingsFile);
#take the aws access and secret key
while(<$settings>){
	if($. == 1){
	my @arrA = split( /"/, $_) ;
	$awsAccess = $arrA[1];	
}	elsif($. == 2){
	my @arrS = split (/"/, $_) ;
	$awsSecret = $arrS[1];	
}
}
#find the info from the command
open(my $writeDns,">>", $dnsFile);
open(my $writeRunning,'>>',$runningFile);
until($isworking=~/yes/){
	my $line = "ec2-describe-instances -O $awsAccess -W $awsSecret";
	my $command= qx{$line};
	@arry =split(/\s+/,$command);
	while($#arry +1){
		my $x=$arry[0];
		 if($x=~/running/){ $isworking="yes";}
		 if($x=~/stopped/){ $isworking="no";}
		 if($x=~/INSTANCE/){ $id=$arry[1];}
		 if($x=~/amazonaws.com/){ $dns=$x;}
		shift(@arry);
}
#if is not working start instance
if($isworking=~/no/){
system("ec2-start-instances -O $awsAccess -W $awsSecret $id"); 
}
}
print $writeRunning  "$id\n" if(!(system("grep $id $runningFile") eq $id));
print $writeDns "$dns\n" if(!(system("grep $dns $dnsFile") eq $dns));

