/********************************************************************************
** Form generated from reading UI file 'addfiledialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDFILEDIALOG_H
#define UI_ADDFILEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_addFileDialog
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *lineEdit;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *addFileDialog)
    {
        if (addFileDialog->objectName().isEmpty())
            addFileDialog->setObjectName(QStringLiteral("addFileDialog"));
        addFileDialog->resize(400, 88);
        layoutWidget = new QWidget(addFileDialog);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(20, 10, 361, 64));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(layoutWidget);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        lineEdit = new QLineEdit(layoutWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        horizontalLayout->addWidget(lineEdit);


        verticalLayout->addLayout(horizontalLayout);

        buttonBox = new QDialogButtonBox(layoutWidget);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(addFileDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), addFileDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), addFileDialog, SLOT(close()));

        QMetaObject::connectSlotsByName(addFileDialog);
    } // setupUi

    void retranslateUi(QDialog *addFileDialog)
    {
        addFileDialog->setWindowTitle(QApplication::translate("addFileDialog", "Add File", 0));
        label->setText(QApplication::translate("addFileDialog", "Full Path of The file", 0));
    } // retranslateUi

};

namespace Ui {
    class addFileDialog: public Ui_addFileDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDFILEDIALOG_H
