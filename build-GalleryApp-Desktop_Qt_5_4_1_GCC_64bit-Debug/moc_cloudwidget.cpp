/****************************************************************************
** Meta object code from reading C++ file 'cloudwidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../GalleryApp/cloudwidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cloudwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_CloudWidget_t {
    QByteArrayData data[12];
    char stringdata[219];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CloudWidget_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CloudWidget_t qt_meta_stringdata_CloudWidget = {
    {
QT_MOC_LITERAL(0, 0, 11), // "CloudWidget"
QT_MOC_LITERAL(1, 12, 12), // "File_request"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 27), // "on_actionSettings_triggered"
QT_MOC_LITERAL(4, 54, 27), // "on_actionAdd_File_triggered"
QT_MOC_LITERAL(5, 82, 29), // "on_actionAdd_Folder_triggered"
QT_MOC_LITERAL(6, 112, 25), // "on_actionExport_triggered"
QT_MOC_LITERAL(7, 138, 25), // "on_actionImport_triggered"
QT_MOC_LITERAL(8, 164, 25), // "on_treeWidget_itemClicked"
QT_MOC_LITERAL(9, 190, 16), // "QTreeWidgetItem*"
QT_MOC_LITERAL(10, 207, 4), // "item"
QT_MOC_LITERAL(11, 212, 6) // "column"

    },
    "CloudWidget\0File_request\0\0"
    "on_actionSettings_triggered\0"
    "on_actionAdd_File_triggered\0"
    "on_actionAdd_Folder_triggered\0"
    "on_actionExport_triggered\0"
    "on_actionImport_triggered\0"
    "on_treeWidget_itemClicked\0QTreeWidgetItem*\0"
    "item\0column"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CloudWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,   50,    2, 0x08 /* Private */,
       4,    0,   51,    2, 0x08 /* Private */,
       5,    0,   52,    2, 0x08 /* Private */,
       6,    0,   53,    2, 0x08 /* Private */,
       7,    0,   54,    2, 0x08 /* Private */,
       8,    2,   55,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 9, QMetaType::Int,   10,   11,

       0        // eod
};

void CloudWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CloudWidget *_t = static_cast<CloudWidget *>(_o);
        switch (_id) {
        case 0: _t->File_request(); break;
        case 1: _t->on_actionSettings_triggered(); break;
        case 2: _t->on_actionAdd_File_triggered(); break;
        case 3: _t->on_actionAdd_Folder_triggered(); break;
        case 4: _t->on_actionExport_triggered(); break;
        case 5: _t->on_actionImport_triggered(); break;
        case 6: _t->on_treeWidget_itemClicked((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (CloudWidget::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CloudWidget::File_request)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject CloudWidget::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_CloudWidget.data,
      qt_meta_data_CloudWidget,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CloudWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CloudWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CloudWidget.stringdata))
        return static_cast<void*>(const_cast< CloudWidget*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int CloudWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void CloudWidget::File_request()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
