/********************************************************************************
** Form generated from reading UI file 'cloudwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLOUDWIDGET_H
#define UI_CLOUDWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CloudWidget
{
public:
    QAction *actionAdd_File;
    QAction *actionAdd_Folder;
    QAction *actionSettings;
    QAction *actionExport;
    QAction *actionImport;
    QAction *actionExit;
    QAction *actionSelect_All;
    QAction *actionCopy;
    QAction *actionCut;
    QAction *actionPaste;
    QAction *actionRemove;
    QAction *actionMerge;
    QAction *actionBy_Name;
    QAction *actionBy_Date;
    QAction *actionSample_First;
    QAction *actionAbout;
    QWidget *centralWidget;
    QTreeWidget *treeWidget;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEdit;
    QMenu *menuArrange;
    QMenu *menuHelp;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *CloudWidget)
    {
        if (CloudWidget->objectName().isEmpty())
            CloudWidget->setObjectName(QStringLiteral("CloudWidget"));
        CloudWidget->resize(931, 661);
        actionAdd_File = new QAction(CloudWidget);
        actionAdd_File->setObjectName(QStringLiteral("actionAdd_File"));
        actionAdd_Folder = new QAction(CloudWidget);
        actionAdd_Folder->setObjectName(QStringLiteral("actionAdd_Folder"));
        actionSettings = new QAction(CloudWidget);
        actionSettings->setObjectName(QStringLiteral("actionSettings"));
        actionExport = new QAction(CloudWidget);
        actionExport->setObjectName(QStringLiteral("actionExport"));
        actionImport = new QAction(CloudWidget);
        actionImport->setObjectName(QStringLiteral("actionImport"));
        actionExit = new QAction(CloudWidget);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        actionSelect_All = new QAction(CloudWidget);
        actionSelect_All->setObjectName(QStringLiteral("actionSelect_All"));
        actionCopy = new QAction(CloudWidget);
        actionCopy->setObjectName(QStringLiteral("actionCopy"));
        actionCut = new QAction(CloudWidget);
        actionCut->setObjectName(QStringLiteral("actionCut"));
        actionPaste = new QAction(CloudWidget);
        actionPaste->setObjectName(QStringLiteral("actionPaste"));
        actionRemove = new QAction(CloudWidget);
        actionRemove->setObjectName(QStringLiteral("actionRemove"));
        actionMerge = new QAction(CloudWidget);
        actionMerge->setObjectName(QStringLiteral("actionMerge"));
        actionBy_Name = new QAction(CloudWidget);
        actionBy_Name->setObjectName(QStringLiteral("actionBy_Name"));
        actionBy_Date = new QAction(CloudWidget);
        actionBy_Date->setObjectName(QStringLiteral("actionBy_Date"));
        actionSample_First = new QAction(CloudWidget);
        actionSample_First->setObjectName(QStringLiteral("actionSample_First"));
        actionAbout = new QAction(CloudWidget);
        actionAbout->setObjectName(QStringLiteral("actionAbout"));
        centralWidget = new QWidget(CloudWidget);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setMaximumSize(QSize(1000, 800));
        treeWidget = new QTreeWidget(centralWidget);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(0, QStringLiteral("1"));
        treeWidget->setHeaderItem(__qtreewidgetitem);
        treeWidget->setObjectName(QStringLiteral("treeWidget"));
        treeWidget->setGeometry(QRect(11, -5, 180, 601));
        treeWidget->setMaximumSize(QSize(180, 750));
        treeWidget->header()->setVisible(false);
        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setGeometry(QRect(200, 0, 721, 591));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 719, 589));
        gridLayoutWidget = new QWidget(scrollAreaWidgetContents);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(0, 0, 721, 591));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        scrollArea->setWidget(scrollAreaWidgetContents);
        CloudWidget->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(CloudWidget);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 931, 25));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuEdit = new QMenu(menuBar);
        menuEdit->setObjectName(QStringLiteral("menuEdit"));
        menuArrange = new QMenu(menuBar);
        menuArrange->setObjectName(QStringLiteral("menuArrange"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QStringLiteral("menuHelp"));
        CloudWidget->setMenuBar(menuBar);
        mainToolBar = new QToolBar(CloudWidget);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        CloudWidget->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(CloudWidget);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        CloudWidget->setStatusBar(statusBar);
        toolBar = new QToolBar(CloudWidget);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        CloudWidget->addToolBar(Qt::TopToolBarArea, toolBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEdit->menuAction());
        menuBar->addAction(menuArrange->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionAdd_File);
        menuFile->addAction(actionAdd_Folder);
        menuFile->addAction(actionSettings);
        menuFile->addAction(actionExport);
        menuFile->addAction(actionImport);
        menuFile->addAction(actionExit);
        menuEdit->addAction(actionSelect_All);
        menuEdit->addAction(actionCopy);
        menuEdit->addAction(actionCut);
        menuEdit->addAction(actionPaste);
        menuEdit->addAction(actionRemove);
        menuEdit->addAction(actionMerge);
        menuArrange->addAction(actionBy_Name);
        menuArrange->addAction(actionBy_Date);
        menuArrange->addAction(actionSample_First);
        menuHelp->addAction(actionAbout);

        retranslateUi(CloudWidget);
        QObject::connect(actionExit, SIGNAL(triggered()), CloudWidget, SLOT(close()));

        QMetaObject::connectSlotsByName(CloudWidget);
    } // setupUi

    void retranslateUi(QMainWindow *CloudWidget)
    {
        CloudWidget->setWindowTitle(QApplication::translate("CloudWidget", "Gallery", 0));
        actionAdd_File->setText(QApplication::translate("CloudWidget", "Add File", 0));
        actionAdd_Folder->setText(QApplication::translate("CloudWidget", "Add Folder", 0));
        actionSettings->setText(QApplication::translate("CloudWidget", "Settings", 0));
        actionExport->setText(QApplication::translate("CloudWidget", "Export", 0));
        actionImport->setText(QApplication::translate("CloudWidget", "Import", 0));
        actionExit->setText(QApplication::translate("CloudWidget", "Exit", 0));
        actionSelect_All->setText(QApplication::translate("CloudWidget", "Select All", 0));
        actionCopy->setText(QApplication::translate("CloudWidget", "Copy", 0));
        actionCut->setText(QApplication::translate("CloudWidget", "Cut", 0));
        actionPaste->setText(QApplication::translate("CloudWidget", "Paste", 0));
        actionRemove->setText(QApplication::translate("CloudWidget", "Remove", 0));
        actionMerge->setText(QApplication::translate("CloudWidget", "Merge", 0));
        actionBy_Name->setText(QApplication::translate("CloudWidget", "By Name", 0));
        actionBy_Date->setText(QApplication::translate("CloudWidget", "By Date", 0));
        actionSample_First->setText(QApplication::translate("CloudWidget", "Sample First", 0));
        actionAbout->setText(QApplication::translate("CloudWidget", "About", 0));
        menuFile->setTitle(QApplication::translate("CloudWidget", "File", 0));
        menuEdit->setTitle(QApplication::translate("CloudWidget", "Edit", 0));
        menuArrange->setTitle(QApplication::translate("CloudWidget", "Arrange", 0));
        menuHelp->setTitle(QApplication::translate("CloudWidget", "Help", 0));
        toolBar->setWindowTitle(QApplication::translate("CloudWidget", "toolBar", 0));
    } // retranslateUi

};

namespace Ui {
    class CloudWidget: public Ui_CloudWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLOUDWIDGET_H
