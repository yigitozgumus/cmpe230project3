#ifndef ADDFOLDERDIALOG_H
#define ADDFOLDERDIALOG_H

#include <QDialog>

namespace Ui {
class AddFolderDialog;
}

class AddFolderDialog : public QDialog
{
    Q_OBJECT


public:
    explicit AddFolderDialog(QWidget *parent = 0);
    ~AddFolderDialog();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::AddFolderDialog *ui;
};

#endif // ADDFOLDERDIALOG_H
