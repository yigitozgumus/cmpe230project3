#ifndef ADDFILEDIALOG_H
#define ADDFILEDIALOG_H

#include <QDialog>
#include<QFuture>

namespace Ui {
class addFileDialog;
}

class addFileDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addFileDialog(QWidget *parent = 0);
    ~addFileDialog();

private slots:



    void on_buttonBox_accepted();

private:
    Ui::addFileDialog *ui;
};

#endif // ADDFILEDIALOG_H
