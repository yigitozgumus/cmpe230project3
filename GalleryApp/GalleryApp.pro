#-------------------------------------------------
#
# Project created by QtCreator 2015-05-19T18:40:05
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GalleryApp
TEMPLATE = app


SOURCES += main.cpp\
        cloudwidget.cpp \
    settingsdialog.cpp \
    addfiledialog.cpp \
    addfolderdialog.cpp

HEADERS  += cloudwidget.h \
    settingsdialog.h \
    addfiledialog.h \
    addfolderdialog.h

FORMS    += cloudwidget.ui \
    settingsdialog.ui \
    addfiledialog.ui \
    addfolderdialog.ui
