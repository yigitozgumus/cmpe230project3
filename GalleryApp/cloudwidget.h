#ifndef CLOUDWIDGET_H
#define CLOUDWIDGET_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QImage>
#include <QPixmap>
#include <QTreeWidgetItem>
#include <QGridLayout>

namespace Ui {
class CloudWidget;
}

class CloudWidget : public QMainWindow
{
    Q_OBJECT

signals:
    void File_request();

public:
    explicit CloudWidget(QWidget *parent = 0);
    ~CloudWidget();
    void clearLayout(QLayout *item);


private slots:

    void on_actionSettings_triggered();

    void on_actionAdd_File_triggered();

    void on_actionAdd_Folder_triggered();

    void on_actionExport_triggered();

    void on_actionImport_triggered();


    void on_treeWidget_itemClicked(QTreeWidgetItem *item, int column);

private:
    Ui::CloudWidget *ui;
    QGraphicsScene *scene;
    QImage *imageObject;
    QPixmap *image ;
    QGridLayout *grid;
    QTreeWidgetItem *Header;
};

#endif // CLOUDWIDGET_H
