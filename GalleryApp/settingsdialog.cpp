#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include <QFile>
#include <QStringList>
#include <QTextStream>
#include <QRegExp>

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);
    QFile Settings("./KeysAndSettings.txt");
    QString SettingList;
    if(Settings.open(QIODevice::ReadOnly)){
        QTextStream in(&Settings);
        while(!in.atEnd()){
            SettingList += in.readLine();
        }
    }
    QStringList list = SettingList.split("\"");

    for(int i=0;i<1;i++){
        ui->l1->setText(list[1]);
        ui->l2->setText(list[3]);
        ui->l3->setText(list[5]);
        ui->l4->setText(list[7]);
        ui->l5->setText(list[9]);
        ui->l6->setText(list[11]);
    }
    list.clear();
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::on_pushButton_clicked(){
QStringList settings ;
    settings << ui->l1->text();
    settings << ui->l2->text();
    settings << ui->l3->text();
    settings << ui->l4->text();
    settings << ui->l5->text();
    settings << ui->l6->text();
    QFile setFile("./KeysAndSettings.txt");
    setFile.open(QFile::WriteOnly|QFile::Truncate);
    QString newSet;
    QTextStream out(&setFile);
    newSet = "AWS_ACCESS_KEY=\"" + settings[0] + "\"\n";
    out << newSet;
    newSet = "AWS_SECRET_KEY=\"" + settings[1] + "\"\n";
    out << newSet;
    newSet = "IMAGGA_ACCESS_KEY=\"" + settings[2] + "\"\n";
    out << newSet;
    newSet = "IMAGGA_SECRET_KEY=\"" + settings[3] + "\"\n";
    out << newSet;
    newSet = "label=\"" + settings[4] + "\"\n";
    out << newSet;
    newSet = "sample=\"" + settings[5] + "\"";
    out << newSet;
}
