#include "addfiledialog.h"
#include "ui_addfiledialog.h"
#include <string>
#include <QProcess>

addFileDialog::addFileDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addFileDialog)
{
    ui->setupUi(this);
}

addFileDialog::~addFileDialog()
{
    delete ui;
}



void addFileDialog::on_buttonBox_accepted()
{
    QString folderName = ui->lineEdit->text();
    QString program = "perl gallery.pl --add " + folderName;
    std::string command = program.toStdString();
    system(command.c_str());
}
