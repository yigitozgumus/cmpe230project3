#include "cloudwidget.h"
#include "ui_cloudwidget.h"
#include "addfiledialog.h"
#include "addfolderdialog.h"
#include "settingsdialog.h"
#include <QAction>
#include <QProcess>
#include <QStringList>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <QFile>
#include <QTextStream>
#include<QListWidget>
#include <QListWidgetItem>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QImage>
#include <QLabel>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QFileDialog>
CloudWidget::CloudWidget(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CloudWidget){
    ui->setupUi(this);
    grid = new QGridLayout;
    Header = new QTreeWidgetItem(ui->treeWidget);
    QString Label = "All Labels";
    Header->setText(0,Label);
    //connections
    system("perl startaws.pl");
    system("perl uploadaws.pl Scripts -o Scripts");
}

//connections


CloudWidget::~CloudWidget(){
    delete ui;
    system("perl stopaws.pl");
    system("rm ./Labels/*.txt");
    system("rm AllPictures.txt && touch AllPictures.txt");
     system("rm LabelsOnly.txt && touch LabelsOnly.txt");
}



void CloudWidget::on_actionSettings_triggered(){
    SettingsDialog sdialog;
    sdialog.setModal(true);
    sdialog.exec();

}

void CloudWidget::on_actionAdd_File_triggered()
{
    //addFileDialog fileadd;
    //fileadd.setModal(true);
    //fileadd.exec();
    QString fileName = QFileDialog::getOpenFileName(this,"Open File","/home/");
    QString program = "perl gallery.pl --add " + fileName;
    std::string command = program.toStdString();
    system(command.c_str());
    if(fileName != NULL){
    //ui->listWidget->addItem("All Labels");
     QFile labelFile("LabelsOnly.txt");
     QStringList labels;
     if(labelFile.open(QIODevice::ReadOnly)){
         QTextStream in(&labelFile);
         while(!in.atEnd()){
             labels << in.readLine();
         }
     }
     for(int i =0;i< labels.size();i++){
         QTreeWidgetItem *item = new QTreeWidgetItem;
         item->setText(0,labels[i]);
         Header->addChild(item);
         //ui->listWidget->addItem(labels[i]);

     }

    ui->treeWidget->repaint();
}

}

void CloudWidget::on_actionAdd_Folder_triggered()
{
    QString folderName = QFileDialog::getExistingDirectory(this,"Open Directory","/home/",
                                                           QFileDialog::ShowDirsOnly|
                                                           QFileDialog::DontResolveSymlinks);
    QString program = "perl gallery.pl --add " + folderName;
    std::string command = program.toStdString();
    system(command.c_str());
    if(folderName != NULL){
   // ui->listWidget->addItem("All Labels");
    QFile labelFile("LabelsOnly.txt");
    QStringList labels;
    if(labelFile.open(QIODevice::ReadOnly)){
        QTextStream in(&labelFile);
        while(!in.atEnd()){
            labels << in.readLine();
        }
    }
    for(int i =0;i< labels.size();i++){
        QTreeWidgetItem *item = new QTreeWidgetItem;
        item->setText(0,labels[i]);
        Header->addChild(item);
        //ui->listWidget->addItem(labels[i]);

    }
    ui->treeWidget->repaint();
}
}

void CloudWidget::on_actionExport_triggered(){

}

void CloudWidget::on_actionImport_triggered()
{

}
void CloudWidget::clearLayout(QLayout *layout){
    QLayoutItem *item;
    while(item =layout->takeAt(0)){
//        if(item->layout()){
//            clearLayout(item->layout());
//            delete item->layout();
//        }
        if(item->widget()){
            delete item->widget();
        }
        delete item;
    }
}



void CloudWidget::on_treeWidget_itemClicked(QTreeWidgetItem *item, int column)
{
    QString nameOfLabel = item->text(0);
    std::string test = nameOfLabel.toStdString();
    std::string location;
    if(test == "All Labels"){
        location = "./AllPictures.txt";
        QString srsly(location.c_str());
        QFile pictureFile(srsly);
        QStringList PictureEntries;
        if(pictureFile.open(QIODevice::ReadOnly)){
            QTextStream in(&pictureFile);
            while(!in.atEnd()){
                PictureEntries << in.readLine();
            }
        }
        CloudWidget::clearLayout(ui->gridLayout);
        for(int i=0;i<PictureEntries.size();i++){
            QLabel *label= new QLabel(ui->gridLayoutWidget);
            QPixmap *picture = new QPixmap(PictureEntries[i]);
            //QPixmap *convert = picture->scaled(200,160,Qt::KeepAspectRatio);
            label->setPixmap(picture->scaled(200,160,Qt::KeepAspectRatio));
            ui->gridLayout->addWidget(label,i/3,i%3,Qt::AlignTop);
        }

    }else{
   // std::cout << test << std::endl;
    location = "./Labels/" + test + ".txt";
    QString srsly(location.c_str());
    QFile pictureFile(srsly);
    QStringList PictureEntries;
    if(pictureFile.open(QIODevice::ReadOnly)){
        QTextStream in(&pictureFile);
        while(!in.atEnd()){
            PictureEntries << in.readLine();
        }
    }
    CloudWidget::clearLayout(ui->gridLayout);
    for(int i=0;i<PictureEntries.size();i++){
        QLabel *label= new QLabel(ui->gridLayoutWidget);
        QPixmap picture(PictureEntries[i]);
        QPixmap convert = picture.scaled(200,160,Qt::KeepAspectRatio);
        label->setPixmap(convert);
        ui->gridLayout->addWidget(label,i/3,i%3,Qt::AlignTop);
    }
    }
}
