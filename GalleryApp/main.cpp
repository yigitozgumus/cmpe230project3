#include "cloudwidget.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CloudWidget w;
    w.show();

    return a.exec();
}
