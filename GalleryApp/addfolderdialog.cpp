#include "addfolderdialog.h"
#include "ui_addfolderdialog.h"
#include <string>

AddFolderDialog::AddFolderDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddFolderDialog)
{
    ui->setupUi(this);
}

AddFolderDialog::~AddFolderDialog()
{
    delete ui;
}

void AddFolderDialog::on_buttonBox_accepted()
{
    QString folderName = ui->lineEdit->text();
    QString program = "perl gallery.pl --add " + folderName;
    std::string command = program.toStdString();
    system(command.c_str());
}
